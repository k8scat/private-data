CREATE DATABASE juejin;

CREATE TABLE `article` (
    `id` INT PRIMARY KEY AUTOINCREMENT,
    `article_id` VARCHAR(30) NOT NULL
    `user_id` VARCHAR(30) NOT NULL,
    `category_id` VARCHAR(30) NOT NULL,
    `visible_level` INT NOT NULL DEFAULT 0,
    `link_url` VARCHAR(240) DEFAULT '',
    `is_gfw` INT NOT NULL DEFAULT 0,
    `title` VARCHAR(100) NOT NULL,
    `brief_content` VARCHAR(200) NOT NULL,
    `is_english` INT NOT NULL DEFAULT 0,
    `cover_image` VARCHAR(240) NOT NULL,
);

CREATE TABLE `user` ();

CREATE TABLE `article_tag` ();

CREATE TABLE `tag` ();